# My Internet IP Rust Crate
"My Internet IP" is a small rust crate for retrieving the public IP
of the device this code is executed on.

## Changes in 0.1.1
The code contains no more ``unwrap``, but the return type of ``get``
has been changed to the new enum ``MyIpError`` which wraps all
errors which may occur during retrieving of the IP.

## Example

````rust
extern crate my_internet_ip;

fn main() {
	let ip: ::std::net::IpAddr = match my_internet_ip::get() {
		Ok(ip) => ip,
		Err(e) => panic!("Could not get IP: {}", e)
	};

	// Do something with the IP, e.g. print it
}
````

*Note*: The call to ``my_internet_ip::get()`` blocks until the public IP
was retrieved or an error occurred.

## Cargo.toml
````toml
my_internet_ip = "0.1.1"
````

## How it works
This crate uses the [curl crate](https://crates.io/crates/curl) to
access http://www.myip.ch and parse the result.

## Ideas for contributions
* Support async retrieval of the IP
* Use other sites to retrieve the IP (maybe STUN server?) and select one randomly
* Clear up the code (it is quite convoluted, but it works)

## License

MIT
